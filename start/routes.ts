/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from "@ioc:Adonis/Core/Route";

Route.group(() => {
  // Default
  Route.get("/", async () => {
    return {
      Project:
        "Final Project Sanber Code - Adonis JS Batch 25 ( Aldo Ignata Chandra ) ",
    };
  }).as("landingpage");

  // API => Auth User ( Register, Login &  OTP Confirmation)
  Route.post("/register", "AuthController.register").as("auth.register");
  Route.post("/login", "AuthController.login").as("auth.login");
  Route.post("/otp-confirmation", "AuthController.otpConfirmation").as(
    "auth.otpConfirmation"
  );

  // API => Venue
  Route.resource("venues", "VenuesController")
    .apiOnly()
    .middleware({ "*": "auth" });

  // API => Fields
  Route.resource("fields", "FieldsController")
    .apiOnly()
    .middleware({ "*": "auth" });

  // API => Booking Fields
  Route.post("/fields/:id/bookings", "BookingsController.booking")
    .as("booking.bookings")
    .middleware("auth");

  // API => Booking
  Route.get("/bookings", "BookingsController.index")
    .as("booking.list")
    .middleware("auth");
  Route.get("/bookings/:id", "BookingsController.show")
    .as("booking.data")
    .middleware("auth");
  Route.post("/bookings/:id/join", "BookingsController.join")
    .as("booking.join")
    .middleware("auth");
  Route.post("/bookings/:id/unjoin", "BookingsController.unjoin")
    .as("booking.unjoin")
    .middleware("auth");
  Route.get("/schedules", "BookingsController.schedules")
    .as("booking.schedules")
    .middleware("auth");
  Route.put("/bookings/:id", "BookingsController.update")
    .as("booking.update")
    .middleware("auth");
  Route.delete("/bookings/:id", "BookingsController.delete")
    .as("booking.delete")
    .middleware("auth");
})
  .prefix("/api/v1")
  .as("apiv1");
