<h1 align="center">Final Project - SanberCode Backend Adonis JS<br>( TypeScript )</h1>

<p align="center">
    <img src="./images/Banner.png" width="500"/>
</p>

<p align="center">
    <img src="https://img.shields.io/badge/-typescript-white?style=for-the-badge&logo=typescript">
</p>

<p align="center">
  <img src="https://img.shields.io/badge/luxon-8.0.6-yellow">
  <img src="https://img.shields.io/badge/mysql-2.18.1-blue">
  <img src="https://img.shields.io/badge/bcrypt-1.0.7-brightgreen">
  <img src="https://img.shields.io/badge/swagger-1.3.3-purple">
</p>

<p align="justify">
    This repository contains Final Project - SanberCode Backend using Adonis JS and TypeScript. Have REST API with validation and authentication. 
</p>

## Requirments

- **adonis** v5
- **NodeJS** v14.16.0

## List REST API

- Authentication ( Register, Login, OTP-Confirmation )
- Venues ( Cread, Read, Update, Delete )
- Fields ( Cread, Read, Update, Delete )
- Bookings ( Booking Field, Join Booking, Unjoin Booking, Schedules Booking )

## Link Documentation ( Swagger )

- https://booking-fields-app.herokuapp.com/docs/index.html

## Author

- **name** : Aldo Ignata Chandra
- **email** : aldoignatachandra@gmail.com
