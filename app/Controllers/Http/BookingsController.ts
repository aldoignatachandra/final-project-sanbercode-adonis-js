import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import BookingValidator from "App/Validators/BookingValidator";

// Import Model
import Booking from "App/Models/Booking";
import UserHasBooking from "App/Models/UserHasBooking";

export default class BookingsController {
  // endpoint POST fields/:id/bookings
  public async booking({
    params,
    request,
    response,
    auth,
  }: HttpContextContract) {
    try {
      await request.validate(BookingValidator);

      // Check Is The Same Booking (With Diff User) Already Exist Or Not
      let checkBooking = await Booking.query()
        .where("play_date_start", request.body().play_date_start)
        .where("play_date_end", request.body().play_date_end)
        .where("field_id", params.id)
        .first();

      console.log(checkBooking);

      if (checkBooking) {
        throw new Error(`Failed Booking, Booking Schedule Is Already filled`);
      }

      let newBooking = new Booking();
      await newBooking
        .fill({
          ...request.body(),
          fieldId: params.id,
          userIdBooking: auth.user?.id,
        })
        .save();

      // Create Default Booking Data In Table Pivot
      let userBooking = new UserHasBooking();
      await userBooking
        .fill({ userId: auth.user?.id, bookingId: newBooking.id })
        .save();

      response.status(201).json({
        message: "Success Created New Data Booking Venue",
        data: newBooking,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : GET /bookings
  public async index({ response }: HttpContextContract) {
    try {
      let bookings = await Booking.query().preload("field");

      response.status(200).json({
        message: "Success Displayed All Booking Data",
        data: bookings,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : GET /bookings/:id
  public async show({ params, response }: HttpContextContract) {
    try {
      let booking = await Booking.query()
        .preload("field", (field) => {
          field.select("name", "type");
        })
        .preload("players", (player) => {
          player.select("user_id").preload("user", (user) => {
            user.select("name", "email");
          });
        })
        .where("id", params.id)
        .first();

      if (!booking) {
        throw new Error("Data Not Exist In Database");
      }

      response.status(200).json({
        message: "Success Displayed Selected Booking Data",
        data: booking,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : PUT /bookings/:id/join
  public async join({ auth, params, response }: HttpContextContract) {
    try {
      if (!auth.user) {
        throw new Error("Invalid Credentials");
      }

      // Check That User Already Join Selected Booking Or Not
      let checkUserBooking = await UserHasBooking.query()
        .preload("user")
        .where("user_id", auth.user.id)
        .where("booking_id", params.id)
        .first();

      if (checkUserBooking) {
        throw new Error(
          `User With Name (${checkUserBooking?.user.name}) & Email (${checkUserBooking?.user.email}) Already Join Selected Booking Field`
        );
      }

      let joinBooking = new UserHasBooking();
      await joinBooking
        .fill({ userId: auth.user?.id, bookingId: params.id })
        .save();

      response.status(200).json({
        message: "Success Join Selected Booking Field",
        data: joinBooking,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : PUT /bookings/:id/unjoin
  public async unjoin({ auth, params, response }: HttpContextContract) {
    try {
      if (!auth.user) {
        throw new Error("Invalid Credentials");
      }

      // Check That User Already Join Selected Booking Or Not
      let checkUserBooking = await UserHasBooking.query()
        .preload("user")
        .where("user_id", auth.user.id)
        .where("booking_id", params.id)
        .first();

      if (!checkUserBooking) {
        throw new Error(
          `Failed Unjoin, You Are Not Already Join Selected Booking Field`
        );
      }

      let unjoinBooking = await UserHasBooking.findOrFail(checkUserBooking.id);
      await unjoinBooking.delete();

      response.status(200).json({
        message: "Success Unjoin Selected Booking Field",
        data: unjoinBooking,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : GET /schedules
  public async schedules({ auth, response }: HttpContextContract) {
    try {
      if (!auth.user) {
        throw new Error("Invalid Credentials");
      }

      let bookingJoin = await UserHasBooking.query()
        .preload("booking", (booking) => {
          booking
            .select("field_id", "play_date_start", "play_date_end")
            .preload("field", (field) => {
              field.select("name", "type");
            });
        })
        .select("booking_id")
        .where("user_id", auth.user.id);

      response.status(200).json({
        message: "Success Displayed All Of Your Booking Field",
        data: bookingJoin,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : PUT /bookings/:id
  public async update({
    params,
    request,
    response,
    auth,
  }: HttpContextContract) {
    try {
      // Check User Login (Only Who Has Same user_id_booking or owner can update booking data)
      let dataBooking = await Booking.find(params.id);
      if (auth.user && dataBooking) {
        if (auth.user.id !== dataBooking.userIdBooking) {
          if (auth.user.role !== "owner") {
            throw new Error(
              `Only Owner And User Who Booking The Place Can Update Selected Booking Data`
            );
          }
        }
      }

      await request.validate(BookingValidator);

      let updatedBooking = await Booking.findOrFail(params.id);
      await updatedBooking.merge(request.body()).save();

      response.status(201).json({
        message: "Success Updated Selected Data Booking Field",
        data: updatedBooking,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : DELETE /bookings/:id
  public async delete({ params, response, auth }: HttpContextContract) {
    try {
      // Check User Login (Only Who Has Same user_id_booking or owner can Delete booking data)
      let dataBooking = await Booking.find(params.id);
      if (auth.user && dataBooking) {
        if (auth.user.id !== dataBooking.userIdBooking) {
          if (auth.user.role !== "owner") {
            throw new Error(
              `Only Owner And User Who Booking The Place Can Delete Selected Booking Data`
            );
          }
        }
      }

      let deletedBooking = await Booking.findOrFail(params.id);
      await deletedBooking.delete();

      response.status(201).json({
        message: "Success Deleted Selected Data Booking Field",
        data: deletedBooking,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }
}
