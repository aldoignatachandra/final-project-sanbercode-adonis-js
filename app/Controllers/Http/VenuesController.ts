import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import CreateVenueValidator from "App/Validators/CreateVenueValidator";

// Import Model
import Venue from "App/Models/Venue";

export default class VenuesController {
  // endpoint : POST /venues
  public async store({ request, response, auth }: HttpContextContract) {
    try {
      // Check User AUTH => Role Access
      if (auth.user && auth.user.role !== "owner") {
        throw new Error(`Only User With Role 'owner' Can Create Data Venue`);
      }

      await request.validate(CreateVenueValidator);

      let newVenue = new Venue();
      await newVenue.fill(request.body()).save();

      response
        .status(201)
        .json({ message: "Success Created New Data Venue", data: newVenue });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : GET /venues
  public async index({ request, response, auth }: HttpContextContract) {
    try {
      // Check User AUTH => Role Access
      if (auth.user && auth.user.role !== "owner") {
        throw new Error(`Only User With Role 'owner' Can Access Data Venue`);
      }

      let venues: any;
      if (request.qs().name) {
        venues = await Venue.query()
          .where("name", "LIKE", "%" + request.qs().name + "%")
          .preload("fields");
      } else {
        venues = await Venue.query().preload("fields");
      }

      response
        .status(200)
        .json({ message: "Success Displayed All Data Venue", data: venues });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : GET /venues/:id
  public async show({ params, response, auth }: HttpContextContract) {
    try {
      // Check User AUTH => Role Access
      if (auth.user && auth.user.role !== "owner") {
        throw new Error(`Only User With Role 'owner' Can Access Data Venue`);
      }

      let venue = await Venue.query()
        .where("id", params.id)
        .preload("fields")
        .first();

      if (!venue) {
        throw new Error("Data Not Exist In Database");
      }

      response.status(200).json({
        message: "Success Displayed Selected Data Venue",
        data: venue,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : PUT /venues/:id
  public async update({
    params,
    response,
    request,
    auth,
  }: HttpContextContract) {
    try {
      // Check User AUTH => Role Access
      if (auth.user && auth.user.role !== "owner") {
        throw new Error(`Only User With Role 'owner' Can Update Data Venue`);
      }

      await request.validate(CreateVenueValidator);

      let updatedVenue = await Venue.findOrFail(params.id);
      await updatedVenue.merge(request.body()).save();

      response.status(201).json({
        message: "Success Updated Selected Data Venue",
        data: updatedVenue,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : DELETE /venues/:id
  public async destroy({ params, response, auth }: HttpContextContract) {
    try {
      // Check User AUTH => Role Access
      if (auth.user && auth.user.role !== "owner") {
        throw new Error(`Only User With Role 'owner' Can Delete Data Venue`);
      }

      let deletedVenue = await Venue.findOrFail(params.id);
      await deletedVenue.delete();

      response.status(200).json({
        message: "Success Deleted Selected Data Venue",
        data: deletedVenue,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }
}
