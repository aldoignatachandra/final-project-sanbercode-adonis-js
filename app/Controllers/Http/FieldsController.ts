import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import CreateFieldValidator from "App/Validators/CreateFieldValidator";

// Import Model
import Field from "App/Models/Field";

export default class FieldsController {
  // endpoint : POST /fields
  public async store({ request, response, auth }: HttpContextContract) {
    try {
      // Check User AUTH => Role Access
      if (auth.user && auth.user.role !== "owner") {
        throw new Error(`Only User With Role 'owner' Can Create Data Field`);
      }

      await request.validate(CreateFieldValidator);

      let newField = new Field();
      await newField.fill(request.body()).save();

      response
        .status(201)
        .json({ message: "Success Created New Data Field", data: newField });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : GET /fields
  public async index({ response }: HttpContextContract) {
    try {
      let field = await Field.query().preload("venue");

      response
        .status(200)
        .json({ message: "Success Displayed All Fields", data: field });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : GET /fields/:id
  public async show({ params, response }: HttpContextContract) {
    try {
      let field = await Field.query()
        .preload("venue")
        .preload("bookings")
        .where("id", params.id)
        .first();

      if (!field) {
        throw new Error("Data Not Exist In Database");
      }

      response
        .status(200)
        .json({ message: "Success Displayed Selected Field", data: field });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : PUT /fields/:id
  public async update({
    params,
    response,
    request,
    auth,
  }: HttpContextContract) {
    try {
      // Check User AUTH => Role Access
      if (auth.user && auth.user.role !== "owner") {
        throw new Error(`Only User With Role 'owner' Can Update Data Field`);
      }

      await request.validate(CreateFieldValidator);

      let updatedField = await Field.findOrFail(params.id);
      await updatedField.merge(request.body()).save();

      response.status(201).json({
        message: "Success Updated Selected Field",
        data: updatedField,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : DELETE /fields/:id
  public async destroy({ params, response, auth }: HttpContextContract) {
    try {
      // Check User AUTH => Role Access
      if (auth.user && auth.user.role !== "owner") {
        throw new Error(`Only User With Role 'owner' Can Delete Data Field`);
      }

      let deletedField = await Field.findOrFail(params.id);
      await deletedField.delete();

      response.status(200).json({
        message: "Success Deleted Selected Field",
        data: deletedField,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }
}
