import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import UserValidator from "App/Validators/UserValidator";
import { schema } from "@ioc:Adonis/Core/Validator";
import Mail from "@ioc:Adonis/Addons/Mail";

// Import Model
import User from "App/Models/User";
import OtpCode from "App/Models/OtpCode";

export default class AuthController {
  // endpoint : POST /register
  public async register({ request, response }: HttpContextContract) {
    try {
      await request.validate(UserValidator);

      let newUser = new User();
      await newUser.fill(request.body()).save();

      // Random OTP Code
      const otp_code = Math.floor(100000 + Math.random() * 900000);

      let saveCode = new OtpCode();
      await saveCode.fill({ otpCode: otp_code, userId: newUser.id }).save();

      await Mail.send((message) => {
        message
          .from("aldoignatachandra@sanberdev.com")
          .to(request.body().email)
          .subject("OTP Verification!")
          .htmlView("email/otp_verification", { otp_code });
      });

      response.status(201).json({
        message: "Register Success, Please Verify Your OTP Code In Your Email",
        data: newUser,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : POST /login
  public async login({ request, response, auth }: HttpContextContract) {
    try {
      const userSchema = schema.create({
        email: schema.string(),
        password: schema.string(),
      });

      await request.validate({ schema: userSchema });
      const email = request.input("email");
      const password = request.input("password");

      // Check User Email Is Already Verified Or Not
      let user = await User.findBy("email", email);
      if (user && user.isVerified === 0) {
        throw new Error(
          `User With Email ( ${user.email} ) Is Not Verified Yet`
        );
      }

      const token = await auth
        .use("api")
        .attempt(email, password, { expiresIn: "7days" });

      response.status(201).json({
        message: `Success Login User With Email (${email})`,
        token,
      });
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }

  // endpoint : POST /otp-confirmation
  public async otpConfirmation({ request, response }: HttpContextContract) {
    try {
      let otp_code = request.input("otp_code");
      let email = request.input("email");

      let user = await User.findBy("email", email);
      let otpCheck = await OtpCode.findBy("otp_code", otp_code);

      if (user && user?.id === otpCheck?.userId) {
        user.isVerified = 1;
        await user.save();

        response.status(200).json({
          message: "Success OTP Confirmation",
          data: { user: user.name, email },
        });
      } else {
        response.status(400).json({
          message: "Failed OTP Confirmation",
          data: { email },
        });
      }
    } catch (error) {
      response.badRequest({
        errors: error.messages ? error.messages : error.message,
      });
    }
  }
}
