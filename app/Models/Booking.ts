import { DateTime } from "luxon";
import {
  BaseModel,
  column,
  BelongsTo,
  belongsTo,
  hasMany,
  HasMany,
} from "@ioc:Adonis/Lucid/Orm";
import Field from "App/Models/Field";
import UserHasBooking from "App/Models/UserHasBooking";

export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column.dateTime()
  public playDateStart: DateTime;

  @column.dateTime()
  public playDateEnd: DateTime;

  @column()
  public userIdBooking: number;

  @column()
  public fieldId: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @belongsTo(() => Field)
  public field: BelongsTo<typeof Field>;

  @hasMany(() => UserHasBooking)
  public players: HasMany<typeof UserHasBooking>;
}
