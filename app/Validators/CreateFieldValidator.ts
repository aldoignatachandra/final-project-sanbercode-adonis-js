import { schema, rules } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class CreateFieldValidator {
  constructor(protected ctx: HttpContextContract) {}

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    name: schema.string({}, [rules.minLength(4)]),
    type: schema.enum([
      "futsal",
      "voli",
      "basket",
      "minisoccer",
      "soccer",
    ] as const),
    venue_id: schema.number([rules.unsigned()]),
  });

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages = {
    required:
      "Data Input ( {{field}} ) Is Required To Create New / Update Field",
    "venue_id.unsigned": "Must Contains A Venue Parent ID",
    "name.minLength": "Data Input ( {{field}} ) Min 4 Characters",
    "type.enum":
      "Input Type Only 'futsal', 'voli', 'basket', 'mini soccer' or 'soccer'",
  };
}
