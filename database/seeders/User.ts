import BaseSeeder from "@ioc:Adonis/Lucid/Seeder";
import User from "App/Models/User";

export default class UserSeeder extends BaseSeeder {
  public async run() {
    await User.createMany([
      {
        name: "",
        email: "ignata@gmail.com",
        password: "ignata",
        role: "owner",
        is_verified: 1,
      },
      {
        name: "",
        email: "chandra@gmail.com",
        password: "chandra",
        role: "user",
        is_verified: 1,
      },
    ]);
  }
}
