import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class UsersHasBookings extends BaseSchema {
  protected tableName = "users_has_bookings";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id").primary();
      table
        .integer("user_id")
        .unsigned()
        .references("users.id")
        .onDelete("cascade")
        .notNullable();
      table
        .integer("booking_id")
        .unsigned()
        .references("bookings.id")
        .onDelete("cascade")
        .notNullable();
      table.timestamps(true, true);
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
