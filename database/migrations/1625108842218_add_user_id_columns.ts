import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Venues extends BaseSchema {
  protected tableName = "venues";

  public async up() {
    this.schema.table(this.tableName, (table) => {
      table.integer("user_id").unsigned().references("users.id");
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropColumn("user_id");
    });
  }
}
